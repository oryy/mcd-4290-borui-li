    var Obj={
        number:1,
        string:"abc",
        array:[5,4,3,2,1],
        boolean:true
    };

function objectToHTML(){
    let retS="";
    for (let prop in Obj){
        retS+=prop+": "+Obj[prop]+"<br/>"
    }
    return retS;
}

let output="";
output+=objectToHTML();
let outputAreaRef1=document.getElementById("outputarea1");
outputAreaRef1.innerHTML=output;